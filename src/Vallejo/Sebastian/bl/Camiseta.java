package Vallejo.Sebastian.bl;

public class Camiseta {

    private int id;
    private String color;
    private String tamanno;
    private String descripcion;
    private int precio;


    //constructores
    public Camiseta() {
    }

    public Camiseta(int id, String color, String tamanno, String descripcion, int precio) {
        this.id = id;
        this.color = color;
        this.tamanno = tamanno;
        this.descripcion = descripcion;
        this.precio = precio;
    }


    //getters & setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTamanno() {
        return tamanno;
    }

    public void setTamanno(String tamanno) {
        this.tamanno = tamanno;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }


    @Override
    public String toString() {
        return "Camiseta{" +
                "id=" + id +
                ", color='" + color + '\'' +
                ", tamanno='" + tamanno + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", precio=" + precio +
                '}';
    }

}
