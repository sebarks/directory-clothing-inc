package Vallejo.Sebastian.bl;

import java.util.ArrayList;

public class ClientesBL {
    ArrayList<Cliente> clientes;

    public ClientesBL(){
        clientes = new ArrayList<>();
    }

    public void agregarCliente(Cliente cliente){
        clientes.add(cliente);
    }

    public String buscarCliente(int id){
        for (Cliente clienteTemporal : clientes){
            if(id == clienteTemporal.getId()){
                return clienteTemporal.toString();
            }
        }return null;
    }


    public String[] listarCliente(){
        String[] datosCliente = new String[clientes.size()];
        for(int i = 0 ; i< clientes.size(); i ++){
            datosCliente[i] = clientes.get(i).toString();
        }
        return datosCliente;
    }

}
