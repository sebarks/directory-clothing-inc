package Vallejo.Sebastian.bl;

import java.util.ArrayList;

public class CatalogoBL {
    ArrayList<Catalogo> catalogos;

    public CatalogoBL(){
        catalogos = new ArrayList<>();
    }

    public void agregarCatalogo(Catalogo catalogo){
        catalogos.add(catalogo);
    }

    public String buscarCatalogo(int id){
        for (Catalogo catalogoTemporal : catalogos){
            if(id == catalogoTemporal.getId()){
                return catalogoTemporal.toString();
            }
        }return null;
    }

    public String[] listarCatalogo(){
        String[] datosCatalogo = new String[catalogos.size()];
        for(int i = 0 ; i< catalogos.size(); i ++){
            datosCatalogo[i] = catalogos.get(i).toString();
        }
        return datosCatalogo;
    }

}
