package Vallejo.Sebastian.bl;

public class Catalogo {

    private int id;
    private String fechaCreacion;
    private String nombre;

    //constructores


    public Catalogo() {
    }

    public Catalogo(int id, String fechaCreacion, String nombre) {
        this.id = id;
        this.fechaCreacion = fechaCreacion;
        this.nombre = nombre;
    }

    //getters & setters


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Catalogo{" +
                "id=" + id +
                ", fechaCreacion='" + fechaCreacion + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
