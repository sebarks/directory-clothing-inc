package Vallejo.Sebastian.bl;

import java.util.ArrayList;

public class CamisetaBL {
    ArrayList<Camiseta> camisetas;

    public CamisetaBL(){
        camisetas = new ArrayList<>();
    }

    public void agregarCamiseta(Camiseta camiseta){
        camisetas.add(camiseta);
    }

    public String buscarCamiseta(int id){
        for (Camiseta camisetaTemporal : camisetas){
            if(id == camisetaTemporal.getId()){
                return camisetaTemporal.toString();
            }
        }return null;
    }

    public String[] listarCamiseta(){
        String[] datosCamiseta = new String[camisetas.size()];
        for(int i = 0 ; i< camisetas.size(); i ++){
            datosCamiseta[i] = camisetas.get(i).toString();
        }
        return datosCamiseta;
    }

}
