package Vallejo.Sebastian.ui;

import Vallejo.Sebastian.bl.*;
import Vallejo.Sebastian.tl.CamisetaController;
import Vallejo.Sebastian.tl.CatalogoController;
import Vallejo.Sebastian.tl.ClienteController;

import java.io.BufferedReader;
import java.io.IOException;

import java.io.InputStreamReader;
import java.util.ArrayList;


public class UI {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static ClienteController gestor1 = new ClienteController();
    static CamisetaController gestor2 = new CamisetaController();
    static CatalogoController gestor3 = new CatalogoController();


    public static void main(String[] args) throws IOException {

        mostrarMenu();
    }

    public static void mostrarMenu() throws IOException {
        int opcion = 0;

        do {
            System.out.println("*** bienvenido al sistema ***");
            System.out.println("1. Registrar Cliente");
            System.out.println("2. Registrar Camiseta");
            System.out.println("3. Registrar Catalogo");
            System.out.println("4. Listar Cliente");
            System.out.println("5. Listar Camiseta");
            System.out.println("6. Listar Catalogo");
            System.out.println("7. Salir");
            System.out.println("digite la opcion que desee: ");
            opcion = Integer.parseInt(in.readLine());

            procesarOpcion(opcion);

        } while (opcion != 7);
    }

    public static void procesarOpcion(int opcion) throws IOException {
        switch (opcion) {
            case 1:
                registrarCliente();
                break;
            case 2:
                registrarCamiseta();
                break;
            case 3:
                registrarCatalogo();
                break;
            case 4:
                listarCliente();
                break;
            case 5:
                listarCamiseta();
                break;
            case 6:
                listarCatalogo();
                break;
            case 7:
                System.exit(0);
            default:
                System.out.println("digite una opcion valida");
                break;

        }
    }

//*******************************************************************Cliente**********************************************************************************

    static void registrarCliente() throws IOException {
        System.out.println("***Registrar cliente***");
        System.out.println("ingrese el id");
        int id = Integer.parseInt(in.readLine());
        System.out.println("ingrese el nombre");
        String nombre = in.readLine();
        System.out.println(" ingrese el primer apellido ");
        String apellido1 = in.readLine();
        System.out.println(" ingrese el segundo apellido ");
        String apellido2 = in.readLine();
        System.out.println(" ingrese la direccion ");
        String direccion = in.readLine();
        System.out.println("ingrese el correo");
        String correo = in.readLine();


        if (gestor1.buscarCliente(id) == null) {
            gestor1.registrarCliente(id, nombre, apellido1, apellido2, direccion, correo);
            System.out.println("Cliente registrado");
        } else {
            System.out.println("El cliente " + nombre + " con el id " + id + " ya esta registrado ");
        }
    }


    public static void listarCliente() {
        System.out.println("****LISTAR CLIENTES****");
        String[] infoClientes = gestor1.listarCliente();
        for (int i = 0; i < infoClientes.length; i++) {
            System.out.println(infoClientes[i]);
        }
    }

//    public static void buscarCliente() throws IOException {
//        System.out.println("BUSCAR CLIENTE");
//        System.out.println("digite el id del cliente");
//        int id = Integer.parseInt(in.readLine());
//
//        String cliente = gestor1.buscarCliente(id);
//
//        if (cliente == null) {
//            System.out.println("Cliente no registrado");
//        } else {
//            System.out.println(cliente);
//        }
//
//
//    }
//*******************************************************************Cliente**********************************************************************************

    //*******************************************************************Catalogo**********************************************************************************
    public static void registrarCatalogo() throws IOException {
        System.out.println("ingrese el id");
        int id = Integer.parseInt(in.readLine());
        System.out.println("ingrese la fecha");
        String fechaCreacion = in.readLine();
        System.out.println("ingrese el nombre");
        String nombre = in.readLine();

        if (gestor3.buscarCatalogo(id) == null) {
            gestor3.registrarCatalogo(id, fechaCreacion, nombre);
            System.out.println("Catalogo registrado");
        } else {
            System.out.println("El Catalogo " + nombre + " con el id " + id + " ya esta registrado ");
        }
    }


    public static void listarCatalogo() {
        System.out.println("****LISTAR CATALOGOS****");
        String[] infoCatalogos = gestor3.listarCatalogo();
        for (int i = 0; i < infoCatalogos.length; i++) {
            System.out.println(infoCatalogos[i]);
        }
    }

//    public static void buscarCatalogo() throws IOException {
//        System.out.println("digite el id del catalogo");
//        int id = Integer.parseInt(in.readLine());
//
//        String catalogo = gestor3.buscarCatalogo(id);
//        if (catalogo == null){
//            System.out.println("Catalogo no registrado");
//        }else{
//            System.out.println(catalogo);
//        }
//    }
//*******************************************************************Catalogo**********************************************************************************


    //*******************************************************************Camiseta**********************************************************************************
    public static void registrarCamiseta() throws IOException {
        System.out.println("ingrese el id");
        int id = Integer.parseInt(in.readLine());
        System.out.println("ingrese el color");
        String color = in.readLine();
        System.out.println(" ingrese la Talla ");
        String tamanno = in.readLine();
        System.out.println(" ingrese una descripcion ");
        String descripcion = in.readLine();
        System.out.println(" ingrese el precio ");
        int precio = Integer.parseInt(in.readLine());


        if (gestor2.buscarCamiseta(id) == null) {
            gestor2.registrarCamiseta(id, color, tamanno, descripcion, precio);
            System.out.println("Camiseta registrada");
        } else {
            System.out.println("La Camiseta " + color + " con el id " + id + " ya esta registrada ");
        }

    }


    public static void listarCamiseta() {
        System.out.println("****LISTAR CAMISETAS****");
        String[] infoCamisetas = gestor2.listarCamiseta();
        for (int i = 0; i < infoCamisetas.length; i++) {
            System.out.println(infoCamisetas[i]);
        }
    }

//    public static void buscarCamiseta() throws IOException {
//        System.out.println("digite el id de la camiseta");
//        int id = Integer.parseInt(in.readLine());
//
//        String camiseta = gestor2.buscarCamiseta(id);
//        if (camiseta == null){
//            System.out.println("Camiseta no registrado");
//        }else{
//            System.out.println(camiseta);
//        }
//
//
//    }
//*******************************************************************Camiseta**********************************************************************************

}
