package Vallejo.Sebastian.tl;

import Vallejo.Sebastian.bl.Camiseta;
import Vallejo.Sebastian.bl.CamisetaBL;
import Vallejo.Sebastian.bl.Cliente;

public class CamisetaController {
    private CamisetaBL logica = new CamisetaBL();

    public void registrarCamiseta(int id, String color, String tamanno, String descripcion, int precio){
        Camiseta camiseta = new Camiseta(  id,  color,  tamanno,  descripcion,  precio);
        logica.agregarCamiseta(camiseta);
    }

    public String  buscarCamiseta(int id){
        return logica.buscarCamiseta(id);
    }

    public String[] listarCamiseta(){
        return logica.listarCamiseta();
    }
}
