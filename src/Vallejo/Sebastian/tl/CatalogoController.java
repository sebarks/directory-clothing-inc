package Vallejo.Sebastian.tl;

import Vallejo.Sebastian.bl.Catalogo;
import Vallejo.Sebastian.bl.CatalogoBL;
import Vallejo.Sebastian.bl.Cliente;

public class CatalogoController {
    private CatalogoBL logica = new CatalogoBL();

    public void registrarCatalogo(int id, String fechaCreacion, String nombre){
        Catalogo catalogo = new Catalogo( id,  fechaCreacion,  nombre);
        logica.agregarCatalogo(catalogo);
    }

    public String  buscarCatalogo(int id){
        return logica.buscarCatalogo(id);
    }

    public String[] listarCatalogo(){
        return logica.listarCatalogo();
    }
}
