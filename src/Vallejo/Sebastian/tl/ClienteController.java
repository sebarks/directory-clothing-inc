package Vallejo.Sebastian.tl;

import Vallejo.Sebastian.bl.Cliente;
import Vallejo.Sebastian.bl.ClientesBL;

public class ClienteController {
    private ClientesBL logica = new ClientesBL();

    public void registrarCliente(int id, String nombre, String apellido1, String apellido2, String direccion, String correo){
        Cliente cliente = new Cliente( id,  nombre,  apellido1,  apellido2,  direccion,  correo);
        logica.agregarCliente(cliente);
    }

    public String  buscarCliente(int id){
        return logica.buscarCliente(id);
    }

    public String[] listarCliente(){
        return logica.listarCliente();
    }
}
